from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.

def index(request):

	grupo1 = Estudiante.objects.filter(grupo=1)
	grupo4 = Estudiante.objects.filter(grupo=4)
	allStudents = Estudiante.objects.filter()
	last_names_to_students = {}
	for student in allStudents:
		if student.apellidos in last_names_to_students:
			last_names_to_students[student.apellidos].append(student)
		else:
			last_names_to_students[student.apellidos] = [student]
	ages_to_students = {}
	for student in allStudents:
		if student.edad in ages_to_students:
			ages_to_students[student.edad].append(student)
		else:
			ages_to_students[student.edad] = [student]
	grupo3 = Estudiante.objects.filter(grupo=3)
	ages_to_group3_students = {}
	for student in grupo3:
		if student.edad in ages_to_group3_students:
			ages_to_group3_students[student.edad].append(student)
		else:
			ages_to_group3_students[student.edad] = [student]
	

	return render(request, 'index.html', {'grupo1':grupo1, 'grupo4':grupo4, 'last_names_to_students':last_names_to_students, 'ages_to_students':ages_to_students, 'allStudents':allStudents, 'ages_to_group3_students':ages_to_group3_students})
	
